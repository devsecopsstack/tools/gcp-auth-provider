import os
import re

from starlette.applications import Starlette
from starlette.exceptions import HTTPException
from starlette.requests import Request
from starlette.responses import PlainTextResponse
from starlette.routing import Route
import uvicorn

from gcp_auth_provider.gcp_client import get_iam_credentials, get_sts_token

# app = FastAPI()


def guess_env_type() -> str:
    env_type = os.getenv("ENV_TYPE")
    if env_type:
        return env_type
    # guess from GitLab CI predefined vars
    ref_name = os.getenv("CI_COMMIT_REF_NAME", "-")
    prod_ref = os.getenv("PROD_REF", "/^(master|main)$/").strip("/")
    if re.match(prod_ref, ref_name):
        # could be staging or prod
        if os.getenv("CI_JOB_STAGE", "-") in [
            "publish",
            "infra-prod",
            "production",
            ".post",
        ]:
            return "production"
        return "staging"

    integ_ref = os.getenv("INTEG_REF", "/^develop$/").strip("/")
    if re.match(integ_ref, ref_name):
        return "integration"

    return "review"


def get_var_prefix(env_type: str) -> str:
    if env_type == "integration":
        return "INTEG"
    if env_type == "production":
        return "PROD"
    return env_type.upper()


def get_oidc_account(var_prefix: str) -> str:
    return os.getenv(f"GCP_{var_prefix}_OIDC_ACCOUNT") or os.getenv("GCP_OIDC_ACCOUNT")


def get_oidc_provider(var_prefix: str) -> str:
    return os.getenv(f"GCP_{var_prefix}_OIDC_PROVIDER") or os.getenv(
        "GCP_OIDC_PROVIDER"
    )


def ping(request: Request):
    return PlainTextResponse("pong")


def token(request: Request):
    workload_identity_provider = request.query_params.get("workloadIdentityProvider")
    service_account = request.query_params.get("serviceAccount")
    env_type = request.query_params.get("envType")

    # projects/%s/locations/global/workloadIdentityPools/%s/providers/%s
    if (not workload_identity_provider) or (not service_account):
        # retrieve from TBC standard variables
        if not env_type:
            env_type = guess_env_type()

        var_prefix = get_var_prefix(env_type)

        workload_identity_provider = get_oidc_provider(var_prefix)
        service_account = get_oidc_account(var_prefix)
        if (not workload_identity_provider) or (not service_account):
            raise HTTPException(
                status_code=400,
                detail=f"Token couldn't retrieve implicit OIDC provider/account for env='{env_type}', workloadIdentityProvider={workload_identity_provider}, service=Account{service_account}",
            )

    audience = f"//iam.googleapis.com/{workload_identity_provider}"

    federated_token = get_sts_token(audience)
    gcloud_auth_token = get_iam_credentials(service_account, federated_token)

    return PlainTextResponse(gcloud_auth_token)


def startup():
    print("Ready to go")


routes = [
    Route("/ping", ping),
    Route("/token", token),
]

app = Starlette(debug=False, routes=routes, on_startup=[startup])

if __name__ == "__main__":
    uvicorn.run(app, host="0.0.0.0", port=8000)
